<?php

namespace App\Tests\Controller;

use App\Tests\ClientService;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthControllerTest extends WebTestCase
{
    /** @var ClientService $client */
    private $clientService = null;

    public function setUp()
    {
        $this->clientService = new ClientService();
    }

    public function testWrongLogin()
    {
        $client = $this->clientService->authClient(
            ClientService::CLIENT_CREDENTIALS[ClientService::WRONG_CLIENT]['username'],
            ClientService::CLIENT_CREDENTIALS[ClientService::WRONG_CLIENT]['password']
        );

        static::assertEquals(401, $client->getResponse()->getStatusCode());
    }

    public function testGoodLogin()
    {
        $client = $this->clientService->authClient(
            ClientService::CLIENT_CREDENTIALS[ClientService::USER_CLIENT]['username'],
            ClientService::CLIENT_CREDENTIALS[ClientService::USER_CLIENT]['password']
        );

        static::assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testIndexWithoutToken()
    {
        $client = $this->clientService->getGuestClient();
        $client->xmlHttpRequest('GET', '/api/hello');

        static::assertEquals(403, $client->getResponse()->getStatusCode());
    }

    public function testIndexWithValidToken()
    {
        $client = $this->clientService->getUserClient();
        $client->xmlHttpRequest('GET', '/api/hello');

        static::assertEquals(200, $client->getResponse()->getStatusCode());
        static::assertEquals(
            [
                "msg" => "Hello user",
            ],
            json_decode($client->getResponse()->getContent(), true)
        );
    }

    public function testIndexWithAdminValidToken()
    {
        $client = $this->clientService->getAdminClient();
        $client->xmlHttpRequest('GET', '/api/hello');

        static::assertEquals(200, $client->getResponse()->getStatusCode());
        static::assertEquals(
            [
                "msg" => "Hello admin",
            ],
            json_decode($client->getResponse()->getContent(), true)
        );

    }
}
