<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;

class ClientService extends WebTestCase
{
    const ADMIN_CLIENT = 'admin_client';
    const USER_CLIENT = 'user_client';
    const WRONG_CLIENT = 'wrong_client';
    const GUEST_CLIENT = 'guest_client';

    const CLIENT_CREDENTIALS = [
        self::ADMIN_CLIENT => ["username" => "admin", "password" => "admin"],
        self::USER_CLIENT  => ["username" => "user",  "password" => "user"],
        self::WRONG_CLIENT => ["username" => "fake",  "password" => "fake"],
    ];

    /**
     * Create base client with server config
     *
     * @return Client
     */
    private function createCustomClient(): Client
    {
        return static::createClient(
            [],
            [
                'HTTP_HOST' => 'auth.stark.home',
                'HTTPS'     => true
            ]
        );
    }

    /**
     * Create auth client with token header
     * nb: Need valid login (use USER_CLIENT or ADMIN_CLIENT)
     *
     * @param string $username
     * @param string $password
     * @return Client
     */
    private function createAuthClient(string $username, string $password): Client
    {
        $client = $this->authClient($username, $password);

        $data = json_decode($client->getResponse()->getContent(), true);

        $client = static::createClient();
        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }

    /**
     * Create client with login post
     * $client->getResponse()->getStatusCode() for result
     *
     * @param string $username
     * @param string $password
     * @return Client
     */
    public function authClient(string $username, string $password): Client
    {
        $client = $this->createCustomClient();
        $client->xmlHttpRequest(
            'POST',
            '/api/login_check',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                "username" => $username,
                "password" => $password
            ])
        );

        return $client;
    }

    /**
     * Create admin client with token header
     *
     * @return Client
     */
    public function getAdminClient(): Client
    {
        return $this->createAuthClient(
            self::CLIENT_CREDENTIALS[self::ADMIN_CLIENT]['username'],
            self::CLIENT_CREDENTIALS[self::ADMIN_CLIENT]['password']
        );
    }

    /**
     * Create user client with token header
     *
     * @return Client
     */
    public function getUserClient(): Client
    {
        return $this->createAuthClient(
            self::CLIENT_CREDENTIALS[self::USER_CLIENT]['username'],
            self::CLIENT_CREDENTIALS[self::USER_CLIENT]['password']
        );
    }

    /**
     * Create guest client without token
     *
     * @return Client
     */
    public function getGuestClient(): Client
    {
        return $this->createCustomClient();
    }
}