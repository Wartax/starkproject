<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class User extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $admin = new \App\Entity\User();
        $admin->setUsername("admin");
        $admin->setEmail("admin@test.fr");
        $password = $this->passwordEncoder->encodePassword($admin, "admin");
        $admin->setPassword($password);
        $admin->setRoles(['ROLE_ADMIN', 'ROLE_USER']);
        $manager->persist($admin);

        $user = new \App\Entity\User();
        $user->setUsername("user");
        $user->setEmail("user@test.fr");
        $password = $this->passwordEncoder->encodePassword($user, "user");
        $user->setPassword($password);
        $user->setRoles(['ROLE_USER']);
        $manager->persist($user);

        $manager->flush();
    }
}
