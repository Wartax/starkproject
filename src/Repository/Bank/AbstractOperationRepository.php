<?php

namespace App\Repository\Bank;

use App\Entity\Bank\AbstractOperation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AbstractOperation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractOperation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractOperation[]    findAll()
 * @method AbstractOperation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AbstractOperationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AbstractOperation::class);
    }

//    /**
//     * @return AbstractOperation[] Returns an array of AbstractOperation objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AbstractOperation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
