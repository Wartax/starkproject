<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends AbstractController
{
    /**
     * @Route("/api/hello", name="demo")
     */
    public function index()
    {
        /** @var User $user */
        $user = $this->getUser();

        $data = [
            "msg" => "Hello " . $user->getUsername(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }
}
