<?php

namespace App\EventListener\Jwt;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Translation\TranslatorInterface;

class NotFoundTokenListener
{
    /** @var TranslatorInterface $translator */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param JWTNotFoundEvent    $event
     */
    public function onJWTNotFound(JWTNotFoundEvent $event)
    {
        $data = [
            'code' => 403,
            'message' => [
                $this->translator->trans('MISSING_TOKEN',  [], 'auth')
            ],
        ];

        $response = new JsonResponse($data, 403);

        $event->setResponse($response);
    }
}