<?php

namespace App\EventListener\Jwt;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTInvalidEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationFailureResponse;
use Symfony\Component\Translation\TranslatorInterface;

class InvalidTokenListener
{
    /**
     * @param JWTInvalidEvent $event
     * @param TranslatorInterface $translator
     */
    public function onJWTInvalid(JWTInvalidEvent $event, TranslatorInterface $translator)
    {
        $response = new JWTAuthenticationFailureResponse(
            $translator->trans('INVALID_TOKEN',  [], 'auth'),
            403
        );

        $event->setResponse($response);
    }
}