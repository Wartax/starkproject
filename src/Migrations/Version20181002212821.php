<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181002212821 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bank_abstract_operation (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_D6AFFC3112469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bank_operation (id INT AUTO_INCREMENT NOT NULL, abstract_operation_id INT NOT NULL, date DATETIME NOT NULL, amount INT NOT NULL, is_payed TINYINT(1) NOT NULL, INDEX IDX_6A6CF0F4D7C78CB2 (abstract_operation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE bank_operation_category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bank_abstract_operation ADD CONSTRAINT FK_D6AFFC3112469DE2 FOREIGN KEY (category_id) REFERENCES bank_operation_category (id)');
        $this->addSql('ALTER TABLE bank_operation ADD CONSTRAINT FK_6A6CF0F4D7C78CB2 FOREIGN KEY (abstract_operation_id) REFERENCES bank_abstract_operation (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bank_operation DROP FOREIGN KEY FK_6A6CF0F4D7C78CB2');
        $this->addSql('ALTER TABLE bank_abstract_operation DROP FOREIGN KEY FK_D6AFFC3112469DE2');
        $this->addSql('DROP TABLE bank_abstract_operation');
        $this->addSql('DROP TABLE bank_operation');
        $this->addSql('DROP TABLE bank_operation_category');
    }
}
