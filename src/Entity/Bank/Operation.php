<?php

namespace App\Entity\Bank;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Bank\OperationRepository")
 * @ORM\Table(name="bank_operation")
 */
class Operation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPayed;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bank\AbstractOperation", inversedBy="operation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $abstractOperation;

    public function __construct()
    {
        $this->date = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getIsPayed(): ?bool
    {
        return $this->isPayed;
    }

    public function setIsPayed(bool $isPayed): self
    {
        $this->isPayed = $isPayed;

        return $this;
    }

    public function getAbstractOperation(): ?AbstractOperation
    {
        return $this->abstractOperation;
    }

    public function setAbstractOperation(?AbstractOperation $abstractOperation): self
    {
        $this->abstractOperation = $abstractOperation;

        return $this;
    }
}
