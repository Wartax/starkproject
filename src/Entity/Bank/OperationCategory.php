<?php

namespace App\Entity\Bank;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Bank\OperationCategoryRepository")
 * @ORM\Table(name="bank_operation_category")
 */
class OperationCategory
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bank\AbstractOperation", mappedBy="category")
     */
    private $abstractOperations;

    public function __construct()
    {
        $this->abstractOperations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|AbstractOperation[]
     */
    public function getAbstractOperations(): Collection
    {
        return $this->abstractOperations;
    }

    public function addAbstractOperation(AbstractOperation $abstractOperation): self
    {
        if (!$this->abstractOperations->contains($abstractOperation)) {
            $this->abstractOperations[] = $abstractOperation;
            $abstractOperation->setCategory($this);
        }

        return $this;
    }

    public function removeAbstractOperation(AbstractOperation $abstractOperation): self
    {
        if ($this->abstractOperations->contains($abstractOperation)) {
            $this->abstractOperations->removeElement($abstractOperation);
            // set the owning side to null (unless already changed)
            if ($abstractOperation->getCategory() === $this) {
                $abstractOperation->setCategory(null);
            }
        }

        return $this;
    }
}
