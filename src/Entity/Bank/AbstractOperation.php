<?php

namespace App\Entity\Bank;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Bank\AbstractOperationRepository")
 * @ORM\Table(name="bank_abstract_operation")
 */
class AbstractOperation
{
    const TYPE_RECURRENT = 'recurrent';
    const TYPE_UNIQUE = 'unique';
    const TYPE_SEVERAL = 'several';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, columnDefinition="ENUM('recurrent', 'unique', 'several')")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Bank\OperationCategory", inversedBy="abstractOperations")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Bank\Operation", mappedBy="abstractOperation", orphanRemoval=true)
     */
    private $operation;

    public function __construct()
    {
        $this->operation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, array(self::TYPE_RECURRENT, self::TYPE_SEVERAL, self::TYPE_UNIQUE))) {
            throw new \InvalidArgumentException("Invalid type");
        }

        $this->type = $type;

        return $this;
    }

    public function getCategory(): ?OperationCategory
    {
        return $this->category;
    }

    public function setCategory(?OperationCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|Operation[]
     */
    public function getOperation(): Collection
    {
        return $this->operation;
    }

    public function addOperation(Operation $operation): self
    {
        if (!$this->operation->contains($operation)) {
            $this->operation[] = $operation;
            $operation->setAbstractOperation($this);
        }

        return $this;
    }

    public function removeOperation(Operation $operation): self
    {
        if ($this->operation->contains($operation)) {
            $this->operation->removeElement($operation);
            // set the owning side to null (unless already changed)
            if ($operation->getAbstractOperation() === $this) {
                $operation->setAbstractOperation(null);
            }
        }

        return $this;
    }
}
